#!/bin/sh
set -e

asterisk_version=16.8-cert14

useradd -r -s /bin/false asterisk

mkdir -p /tmp/asterisk
cd /tmp/asterisk
wget http://downloads.asterisk.org/pub/telephony/certified-asterisk/releases/asterisk-certified-${asterisk_version}.tar.gz -O asterisk.orig.tar.gz
tar --strip-components 1 -xvzf asterisk.orig.tar.gz

chmod +x contrib/scripts/install_prereq
contrib/scripts/install_prereq install

chmod +x contrib/scripts/get_mp3_source.sh
contrib/scripts/get_mp3_source.sh

./configure --with-pjproject-bundled
cp /opt/asterisk-build-scripts/menuselect.makeopts /tmp/asterisk/menuselect.makeopts
make NOISY_BUILD=yes menuselect.makeopts
make install
make samples

chown -R asterisk:asterisk /etc/asterisk
chown -R asterisk:asterisk /var/lib/asterisk

rm -rf /tmp/asterisk
